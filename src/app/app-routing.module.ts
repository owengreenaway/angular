import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { MenuComponent } from './menu/menu.component';
import { ParentComponent } from './nested-routing/parent/parent.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { ChildOneComponent } from './nested-routing/child-one/child-one.component';
import { ChildTwoComponent } from './nested-routing/child-two/child-two.component';
import { BasicComponent } from './binding/basic/basic.component';
import { TemplateDrivenFormComponent } from './binding/template-driven-form/template-driven-form.component';
import { ReactiveFormComponent } from './binding/reactive-form/reactive-form.component';
import { SandboxComponent } from './sandbox/sandbox.component';

const routes: Routes = [
  { path: 'menu', component: MenuComponent },
  {
    path: 'routing',
    component: ParentComponent,
    children: [
      {path: '', redirectTo: 'child1', pathMatch: 'full'},
      { path: 'child1', component: ChildOneComponent },
      { path: 'child2', component: ChildTwoComponent }
    ]
  },
  { path: 'binding', component: BasicComponent },
  { path: 'template-form', component: TemplateDrivenFormComponent },
  { path: 'reactive-form', component: ReactiveFormComponent },
  { path: 'sandbox', component: SandboxComponent },
  { path: '',   redirectTo: '/menu', pathMatch: 'full' },
  { path: '**', component: PageNotFoundComponent },
];

@NgModule({
    imports: [ RouterModule.forRoot(
      routes ,
      { enableTracing: false }
    )
  ],
  exports: [ RouterModule ],
})

export class AppRoutingModule { }
