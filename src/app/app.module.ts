import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MatDatepickerModule, MatFormFieldModule, MatNativeDateModule, MatInputModule } from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { PanelComponent } from './ui/panel/panel.component';
import { AppRoutingModule } from './/app-routing.module';
import { MenuComponent } from './menu/menu.component';
import { ParentComponent } from './nested-routing/parent/parent.component';
import { ChildOneComponent } from './nested-routing/child-one/child-one.component';
import { ChildTwoComponent } from './nested-routing/child-two/child-two.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { BasicComponent } from './binding/basic/basic.component';
import { TemplateDrivenFormComponent } from './binding/template-driven-form/template-driven-form.component';
import { ReactiveFormComponent } from './binding/reactive-form/reactive-form.component';
import { SandboxComponent } from './sandbox/sandbox.component';
import { ButtonComponent } from './ui/button/button.component';
import { PComponent } from './ui/p/p.component';
import { DatepickerComponent } from './ui/datepicker/datepicker.component';


@NgModule({
  declarations: [
    AppComponent,
    PanelComponent,
    MenuComponent,
    ParentComponent,
    ChildOneComponent,
    ChildTwoComponent,
    PageNotFoundComponent,
    BasicComponent,
    TemplateDrivenFormComponent,
    ReactiveFormComponent,
    SandboxComponent,
    ButtonComponent,
    PComponent,
    DatepickerComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatNativeDateModule,
    MatInputModule,
    BrowserAnimationsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
