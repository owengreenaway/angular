import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sandbox',
  templateUrl: './sandbox.component.html',
  styleUrls: ['./sandbox.component.css']
})
export class SandboxComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  handleClick() {
    console.log('sandbox: button was clicked');
  }

  handleDateInput(event) {
    console.log('sandbox: datepicker input:', event.value);
  }

}
