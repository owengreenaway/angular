import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-ui-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent {
  @Input() copy: string;
  @Input() red: boolean;
  @Input() disabled: boolean;
  @Input() isLoading: boolean;
  @Output() onClick: EventEmitter<any> = new EventEmitter();

  handleClick(): void {
    this.onClick.emit();
  }

  className(): string {
    return this.red ? 'button button--red' : 'button';
  }

}
