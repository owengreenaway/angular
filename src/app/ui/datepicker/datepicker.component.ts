import { Component, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-ui-datepicker',
  templateUrl: './datepicker.component.html',
  styleUrls: ['./datepicker.component.scss']
})
export class DatepickerComponent {
  @Output() onDateInput: EventEmitter<any> = new EventEmitter();

  handleDateInput(event) {
    this.onDateInput.emit(event);
  }

}
