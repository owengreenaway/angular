import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-ui-p',
  templateUrl: './p.component.html',
  styleUrls: ['./p.component.scss']
})
export class PComponent {
  @Input() noBottomMargin: boolean;

  className(): string {
    return this.noBottomMargin ? 'p p--no-bottom-margin' : 'p';
  }
}
