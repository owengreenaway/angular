import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-ui-panel',
  templateUrl: './panel.component.html',
  styleUrls: ['./panel.component.scss']
})
export class PanelComponent {
  @Input() title: string;
}
